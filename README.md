# Sweeper

<a href="https://gitlab.com/merzin/sweeper/-/commits/master">
  <img alt="pipeline" src="https://gitlab.com/merzin/sweeper/badges/master/pipeline.svg" />
</a>

Minesweeper clone written in Rust using [Yew](https://yew.rs)
and compiled to WebAssembly.

Available at [sweeper.merzin.dev](https://sweeper.merzin.dev) or
[merzin.gitlab.io/sweeper](https://merzin.gitlab.io/sweeper).

> merzin.gitlab.io/sweeper is outdated since I ran out of CI minutes.

Run as a Chromium app `chromium --app=https://sweeper.merzin.dev`.

> As of Chromium v89.0.4389.90-1 reducing zoom to less than 67% will break the app.

## Local

The following dependencies are required to build and run the application locally.

- [`chromium`](https://www.chromium.org)
- [`miniserve`](https://crates.io/crates/miniserve) or
  [`serve`](https://www.npmjs.com/package/serve)
- [`git`](https://git-scm.com) (download)
- [`wasm-pack`](https://rustwasm.github.io/docs/wasm-pack) (build)
- [`make`](https://www.gnu.org/software/make) (build)

### Download

Download this repository with git.

```
git clone https://gitlab.com/merizn/sweeper
```

Change working directory.

```
cd sweeper
```

### Build

While in the root of the repository you can compile with make.

```
make
```

### Run

While in the root of the repository you can run a shell script which serves
`public` directory (default port: 59234) and opens it in chromium.

```
./sweeper
```
