#[derive(PartialEq, Copy, Clone)]
pub enum Omega {
    Won,
    Lost,
}
