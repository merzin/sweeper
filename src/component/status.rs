use yew::*;

#[derive(Clone, Properties)]
pub struct Props {
    pub onclick: Callback<MouseEvent>,
    pub mines: isize,
    pub ticks: usize,
}

pub struct Status {
    props: Props,
}

impl Component for Status {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="status block-in">
                <span class="counter">
                    {format!("{:04}", self.props.mines)}
                </span>
                <button
                    class="home tile block-out block-button"
                    onclick=self.props.onclick.clone()
                ></button>
                <span class="counter">
                    {format!("{:04}", self.props.ticks)}
                </span>
            </div>
        }
    }
}
