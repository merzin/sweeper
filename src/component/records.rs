use crate::{Difficulty, Record};
use yew::*;

#[derive(Clone, Properties)]
pub struct Props {
    pub onback: Callback<()>,
}

pub struct Records {
    props: Props,
}

impl Component for Records {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="records">
                {Difficulty::variants().iter().map(|&difficulty| html! {
                    <div class="record">
                        <span>{difficulty.as_str()}</span>
                        <span>
                            {Record::load(difficulty).map_or("N/A".into(), |record| record.ticks().to_string())}
                        </span>
                    </div>
                }).collect::<Vec<Html>>()}
                <div class="submit">
                    <button
                        class="block-out block-button"
                        onclick=self.props.onback.reform(|_| ())
                    >{"OK"}</button>
                </div>
            </div>
        }
    }
}
