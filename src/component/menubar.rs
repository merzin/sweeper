use yew::*;

#[derive(Clone, Properties)]
pub struct Props {
    pub options: Vec<String>,
    pub onpick: Callback<usize>,
}

pub struct Menubar {
    props: Props,
}

impl Component for Menubar {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="menubar">
                {for self.props.options.iter().enumerate().map(|(i, op)| html! {
                    <span
                        class="pointer"
                        onclick=self.props.onpick.reform(move |_| i)
                    >{op}</span>
                })}
            </div>
        }
    }
}
