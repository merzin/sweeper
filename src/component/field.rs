use crate::{Omega, Tile};
use yew::*;

fn tile_class(tile: &Tile, omega: Option<Omega>) -> String {
    format!(
        "tile {} {} {}",
        (tile.revealed() || omega == Some(Omega::Lost) && tile.is_mine() && !tile.flagged())
            .then(|| "block-flush")
            .unwrap_or("block-out"),
        tile.revealed()
            .then(|| tile.get_sign().map_or("".into(), |i| format!(
                "sign-{}{}",
                i,
                omega.map(|_| "").unwrap_or(" pointer")
            )))
            .unwrap_or_else(|| format!(
                "{} {}",
                tile.flagged()
                    .then(|| (tile.is_mine() || omega.is_none())
                        .then(|| "flag")
                        .unwrap_or("flag-wrong"))
                    .unwrap_or(""),
                omega.is_some().then(|| "").unwrap_or("block-button")
            )),
        (tile.is_mine() && (tile.revealed() || omega == Some(Omega::Lost) && !tile.flagged()))
            .then(|| format!(
                "mine {}",
                tile.revealed().then(|| "bg-danger").unwrap_or("")
            ))
            .unwrap_or("".into())
    )
}

#[derive(Clone, Properties)]
pub struct Props {
    pub reveal: Callback<usize>,
    pub flag: Callback<usize>,
    pub width: usize,
    pub tiles: Vec<Tile>,
    pub omega: Option<Omega>,
}

pub struct Field {
    props: Props,
}

impl Component for Field {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        html! {
            <div
                class="field block-in-more"
                style=format!("--columns: {}", self.props.width)
            >
                {for self.props.tiles.iter().enumerate().map(|(i, tile)| html! {
                    <span
                        class=tile_class(&tile, self.props.omega)
                        onclick=(self.props.omega.is_none() && (!tile.revealed() || tile.get_sign().is_some()))
                            .then(|| self.props.reveal.reform(move |_| i))
                            .unwrap_or(Callback::noop())
                        oncontextmenu=self.props.omega.is_none()
                            .then(|| tile.revealed()
                                .then(|| self.props.reveal.reform(move |evt: MouseEvent| {
                                    evt.prevent_default();
                                    i
                                }))
                                .unwrap_or_else(|| self.props.flag.reform(move |evt: MouseEvent| {
                                    evt.prevent_default();
                                    i
                                }))
                            )
                            .unwrap_or(Callback::noop())
                    >
                        {tile.revealed()
                            .then(|| tile.get_sign())
                            .flatten()
                            .map_or("".into(), |sign| sign.to_string())}
                    </span>
                })}
            </div>
        }
    }
}
