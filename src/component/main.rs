use crate::component::{Game, Header, Menubar, Records, Settings};
use crate::Difficulty;
use web_sys::Window;
use yew::services::resize::{ResizeService, ResizeTask, WindowDimensions};
use yew::*;

#[derive(PartialEq)]
pub enum Tab {
    Settings,
    Records,
}

impl Tab {
    fn variants() -> Vec<String> {
        vec!["Settings".into(), "Records".into()]
    }

    fn from_usize(i: usize) -> Option<Self> {
        match i {
            0 => Some(Self::Settings),
            1 => Some(Self::Records),
            _ => None,
        }
    }
}

pub enum Msg {
    WindowResize(WindowDimensions),
    Close,
    Menu(Option<Tab>),
    Settings(Difficulty),
}

pub struct Main {
    link: ComponentLink<Self>,
    open_tab: Option<Tab>,
    main_ref: NodeRef,
    resize_listener: Option<(ResizeService, ResizeTask)>,
    difficulty: Difficulty,
}

fn native_border(window: &Window) -> (f64, f64) {
    let inner_width = window.inner_width().ok().map(|v| v.as_f64()).flatten();
    let outer_width = window.outer_width().ok().map(|v| v.as_f64()).flatten();
    let inner_height = window.inner_height().ok().map(|v| v.as_f64()).flatten();
    let outer_height = window.outer_height().ok().map(|v| v.as_f64()).flatten();
    (
        inner_width.map_or(0.0, |inner| outer_width.map_or(0.0, |outer| outer - inner)),
        inner_height.map_or(0.0, |inner| outer_height.map_or(0.0, |outer| outer - inner)),
    )
}

impl Main {
    fn attach_resize_listener(&mut self) {
        let mut resize_service = ResizeService::new();
        let resize_task =
            resize_service.register(self.link.callback(|dims| Msg::WindowResize(dims)));
        self.resize_listener = Some((resize_service, resize_task));
    }

    fn detach_resize_listener(&mut self) {
        self.resize_listener = None;
    }

    fn resize_window(&mut self) {
        self.detach_resize_listener();
        if let Some(elem) = self.main_ref.cast::<web_sys::Element>() {
            let window = utils::window();
            let (border_x, border_y) = native_border(&window);
            let x = (border_x + elem.scroll_width() as f64).ceil() as i32;
            let y = (border_y + elem.scroll_height() as f64).ceil() as i32;
            window.resize_to(x + 1, y + 1).ok();
        }
        self.attach_resize_listener();
    }
}

impl Component for Main {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            open_tab: None,
            difficulty: Difficulty::Easy,
            main_ref: NodeRef::default(),
            resize_listener: None,
        }
    }

    fn rendered(&mut self, _: bool) {
        self.resize_window();
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::WindowResize(_) => {
                self.resize_window();
                false
            }
            Msg::Close => {
                utils::window().close().ok();
                false
            }
            Msg::Menu(tab) => {
                self.open_tab = match tab == self.open_tab {
                    true => None,
                    false => tab,
                };
                true
            }
            Msg::Settings(difficulty) => {
                self.open_tab = None;
                self.difficulty = difficulty;
                true
            }
        }
    }

    fn change(&mut self, _: Self::Properties) -> bool {
        false
    }

    fn view(&self) -> Html {
        html! {
            <main
                class="window"
                ref=self.main_ref.clone()
            >
                <Header
                    title="Minesweeper"
                    onclose=self.link.callback(|_| Msg::Close)
                />
                <Menubar
                    options=Tab::variants()
                    onpick=self.link.callback(|i| Msg::Menu(Tab::from_usize(i)))
                />
                <div class="overlay">
                    <Game difficulty=self.difficulty />
                    {self.open_tab.as_ref().map_or(html! {}, |open_tab| match open_tab {
                        Tab::Settings => html! {
                            <Settings
                                difficulty=self.difficulty
                                onsubmit=self.link.callback(|dlty| Msg::Settings(dlty))
                            />
                        },
                        Tab::Records => html! {
                            <Records
                                onback=self.link.callback(|_| Msg::Menu(None))
                            />
                        },
                    })}
                </div>
            </main>
        }
    }
}
