use crate::component::{Field, Status};
use crate::{Difficulty, Minefield, Omega, Record};
use std::time::Duration;
use yew::services::interval::{IntervalService, IntervalTask};
use yew::*;

pub enum Msg {
    Reveal(usize),
    Flag(usize),
    Menu,
    Tick,
}

#[derive(Default, PartialEq, Clone, Properties)]
pub struct Props {
    pub difficulty: Difficulty,
}

pub struct Game {
    link: ComponentLink<Self>,
    minefield: Minefield,
    omega: Option<Omega>,
    new_record: bool,
    timer: Option<IntervalTask>,
    record: Record,
    props: Props,
}

impl Game {
    fn restart(&mut self, excluding: Option<usize>) {
        self.minefield = Minefield::new(
            self.props.difficulty.width(),
            self.props.difficulty.height(),
            self.props.difficulty.mines(),
            excluding,
        );
        self.omega = None;
        self.new_record = false;
        self.timer = None;
        self.record = Record::new(self.props.difficulty);
    }
}

impl Component for Game {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            minefield: Minefield::new(
                props.difficulty.width(),
                props.difficulty.height(),
                props.difficulty.mines(),
                None,
            ),
            omega: None,
            new_record: false,
            timer: None,
            record: Record::new(props.difficulty),
            props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Reveal(i) => {
                self.omega = self.minefield.reveal(i);
                if self.timer.is_none() {
                    if self.omega.is_none() {
                        self.timer = Some(IntervalService::spawn(
                            Duration::from_secs(1),
                            self.link.callback(|_| Msg::Tick),
                        ));
                    } else {
                        self.restart(Some(i));
                        self.update(msg);
                    }
                } else {
                    if let Some(omega) = self.omega {
                        self.timer = None;
                        if omega == Omega::Won && self.record.save() == Ok(true) {
                            self.new_record = true;
                        }
                    }
                }
            }
            Msg::Flag(i) => self.minefield.toggle_flag(i),
            Msg::Menu => self.restart(None),
            Msg::Tick => self.record.tick(),
        };
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        if self.props != props {
            self.props = props;
            self.restart(None);
            true
        } else {
            false
        }
    }

    fn view(&self) -> Html {
        html! {
            <div
                class=format!(
                    "game block-out {}",
                    match self.omega {
                        Some(Omega::Won) => self.new_record.then(|| "won record").unwrap_or("won"),
                        Some(Omega::Lost) => "lost",
                        None => ""
                    }
                )
            >
                <Status
                    onclick=self.link.callback(move |_| Msg::Menu)
                    mines=(self.minefield.mine_count() as isize - self.minefield.flag_count() as isize)
                    ticks=self.record.ticks()
                />
                <Field
                    reveal=self.link.callback(|i| Msg::Reveal(i)),
                    flag=self.link.callback(|i| Msg::Flag(i)),
                    width=self.minefield.width(),
                    tiles=self.minefield.tiles().to_owned(),
                    omega=self.omega,
                />
            </div>
        }
    }
}
