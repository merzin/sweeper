use yew::*;

#[derive(Clone, Properties)]
pub struct Props {
    pub title: String,
    pub onclose: Callback<MouseEvent>,
}

pub struct Header {
    props: Props,
}

impl Component for Header {
    type Message = ();
    type Properties = Props;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self { props }
    }

    fn update(&mut self, _: Self::Message) -> bool {
        false
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="header">
                <a class="logo" href="https://sweeper.merzin.dev"></a>
                <span>{self.props.title.clone()}</span>
                <button
                    class="block-out block-button close"
                    onclick=self.props.onclose.clone()
                ></button>
            </div>
        }
    }
}
