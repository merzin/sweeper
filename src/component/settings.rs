use crate::Difficulty;
use yew::*;

pub enum Msg {
    Width(usize),
    Height(usize),
    Mines(usize),
    Pick(Difficulty),
}

#[derive(Clone, Properties)]
pub struct Props {
    pub difficulty: Difficulty,
    pub onsubmit: Callback<Difficulty>,
}

pub struct Settings {
    link: ComponentLink<Self>,
    props: Props,
    difficulty: Difficulty,
    width: usize,
    height: usize,
    mines: usize,
}

impl Settings {
    fn sync_from_difficulty(&mut self) {
        self.width = self.difficulty.width();
        self.height = self.difficulty.height();
        self.mines = self.difficulty.mines();
    }

    fn sync_to_difficulty(&mut self) {
        self.difficulty = Difficulty::Custom {
            width: self.width,
            height: self.height,
            mines: self.mines,
        };
    }
}

impl Component for Settings {
    type Message = Msg;
    type Properties = Props;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            difficulty: props.difficulty,
            width: props.difficulty.width(),
            height: props.difficulty.height(),
            mines: props.difficulty.mines(),
            props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> bool {
        match msg {
            Msg::Pick(difficulty) => {
                self.difficulty = difficulty;
                self.sync_from_difficulty();
            }
            Msg::Width(width) => {
                self.width = width;
                self.sync_to_difficulty();
            }
            Msg::Height(height) => {
                self.height = height;
                self.sync_to_difficulty();
            }
            Msg::Mines(mines) => {
                self.mines = mines;
                self.sync_to_difficulty();
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> bool {
        self.props = props;
        false
    }

    fn view(&self) -> Html {
        let old_difficulty = self.props.difficulty;
        let chosen_difficulty = self.difficulty;
        let width = self.width;
        let height = self.height;
        let mines = self.mines;
        html! {
            <div class="settings">
                <div>
                    {Difficulty::variants().iter().map(|&difficulty| html! {
                        <div
                            class="pointer"
                            onclick=self.link.callback(move |_| Msg::Pick(difficulty))
                        >
                            <input
                                type="radio"
                                name="difficulty"
                                value=difficulty.as_str()
                                checked=(difficulty == chosen_difficulty)
                            />
                            <label
                                class="pointer"
                                for=difficulty.as_str()
                            >{difficulty.as_str()}</label>
                        </div>
                    }).collect::<Vec<Html>>()}
                    <div
                        class="pointer"
                        onclick=self.link.callback(move |_| {
                            Msg::Pick(Difficulty::Custom { width, height, mines })
                        })
                    >
                        <input
                            type="radio"
                            name="difficulty"
                            value="Custom"
                            checked=chosen_difficulty.is_custom()
                        />
                        <label
                            class="pointer"
                            for="Custom"
                        >{"Custom"}</label>
                    </div>
                </div>
                <div
                    class=chosen_difficulty.is_custom().then(|| "").unwrap_or("disabled")
                    style="
                        display: grid;
                        grid-gap: 0 0.5rem;
                        margin-left: 1.5rem;
                    "
                >
                    <label for="width">
                        {format!("Width: {}", self.width)}
                    </label>
                    <input
                        type="range"
                        name="width"
                        disabled=!chosen_difficulty.is_custom()
                        step=1
                        min=8
                        max=64
                        value=self.width
                        oninput=self.link
                            .callback(|data: InputData| Msg::Width(data.value.parse().unwrap()))
                    />
                    <label for="height">
                        {format!("Height: {}", self.height)}
                    </label>
                    <input
                        type="range"
                        name="height"
                        disabled=!chosen_difficulty.is_custom()
                        step=1
                        min=8
                        max=64
                        value=self.height
                        oninput=self.link
                            .callback(|data: InputData| Msg::Height(data.value.parse().unwrap()))
                    />
                    <label for="mines">
                        {format!("Mines: {}", self.mines)}
                    </label>
                    <input
                        type="range"
                        name="mines"
                        disabled=!chosen_difficulty.is_custom()
                        step=1
                        min=10
                        max=(self.width * self.height - 1)
                        value=self.mines
                        oninput=self.link
                            .callback(|data: InputData| Msg::Mines(data.value.parse().unwrap()))
                    />
                </div>
                <div class="submit">
                    <button
                        class="block-out block-button"
                        onclick=self.props.onsubmit.reform(move |_| old_difficulty)
                    >{"Cancel"}</button>
                    <button
                        class="block-out block-button"
                        onclick=self.props.onsubmit.reform(move |_| chosen_difficulty)
                    >{"Save"}</button>
                </div>
            </div>
        }
    }
}
