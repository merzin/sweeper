#![recursion_limit = "1024"]

use wasm_bindgen::prelude::wasm_bindgen;

mod adjecent;
mod component;
mod difficulty;
mod minefield;
mod omega;
mod record;
mod tile;

pub use difficulty::Difficulty;
pub use minefield::Minefield;
pub use omega::Omega;
pub use record::Record;
pub use tile::{Tile, TileKind};

#[wasm_bindgen(start)]
pub fn run_app() {
    yew::start_app::<component::Main>();
}
