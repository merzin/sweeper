pub fn top<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    let i = i - width;
    arr.get(i).map(|elem| (i, elem))
}

pub fn bottom<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    let i = i + width;
    arr.get(i).map(|elem| (i, elem))
}

pub fn left<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    (i % width > 0)
        .then(|| {
            let i = i - 1;
            arr.get(i).map(|elem| (i, elem))
        })
        .flatten()
}

pub fn left_top<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    (i % width > 0)
        .then(|| {
            let i = i - 1 - width;
            arr.get(i).map(|elem| (i, elem))
        })
        .flatten()
}

pub fn left_bottom<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    (i % width > 0)
        .then(|| {
            let i = i - 1 + width;
            arr.get(i).map(|elem| (i, elem))
        })
        .flatten()
}

pub fn right<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    (i % width < width - 1)
        .then(|| {
            let i = i + 1;
            arr.get(i).map(|elem| (i, elem))
        })
        .flatten()
}

pub fn right_top<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    (i % width < width - 1)
        .then(|| {
            let i = i + 1 - width;
            arr.get(i).map(|elem| (i, elem))
        })
        .flatten()
}

pub fn right_bottom<T>(arr: &[T], width: usize, i: usize) -> Option<(usize, &T)> {
    (i % width < width - 1)
        .then(|| {
            let i = i + 1 + width;
            arr.get(i).map(|elem| (i, elem))
        })
        .flatten()
}

pub fn all<T>(arr: &[T], width: usize, i: usize) -> [Option<(usize, &T)>; 8] {
    [
        left_top(&arr, width, i),
        left(&arr, width, i),
        left_bottom(&arr, width, i),
        top(&arr, width, i),
        bottom(&arr, width, i),
        right_top(&arr, width, i),
        right(&arr, width, i),
        right_bottom(&arr, width, i),
    ]
}
