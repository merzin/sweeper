use crate::Difficulty;
use serde::{Deserialize, Serialize};
use yew::format::Text;
use yew::services::storage::{Area, StorageService};

#[derive(Serialize, Deserialize)]
pub struct Record {
    difficulty: Difficulty,
    ticks: usize,
}

impl Record {
    pub fn new(difficulty: Difficulty) -> Self {
        Self {
            difficulty,
            ticks: 0,
        }
    }

    pub fn load(difficulty: Difficulty) -> Result<Self, &'static str> {
        let storage = StorageService::new(Area::Local)?;
        let val = storage
            .restore::<Text>(&format!("record_{}", difficulty.as_str()))
            .map_err(|_| "unable to restore")?;
        serde_json::from_str(&val).map_err(|_| "corrupted data")
    }

    pub fn save(&self) -> Result<bool, &'static str> {
        if Record::load(self.difficulty).map_or(true, |old| self.ticks() < old.ticks()) {
            let mut storage = StorageService::new(Area::Local)?;
            let val = serde_json::to_string(self).map_err(|_| "unable to serialize")?;
            storage.store(&format!("record_{}", self.difficulty.as_str()), Ok(val));
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub fn ticks(&self) -> usize {
        self.ticks
    }

    pub fn tick(&mut self) {
        self.ticks += 1
    }
}
