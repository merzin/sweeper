use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Copy, Clone)]
pub enum Difficulty {
    Easy,
    Medium,
    Hard,
    Expert,
    Custom {
        width: usize,
        height: usize,
        mines: usize,
    },
}

impl Default for Difficulty {
    fn default() -> Self {
        Self::Easy
    }
}

impl Difficulty {
    pub fn custom(width: usize, height: usize, mines: usize) -> Self {
        Self::Custom {
            width,
            height,
            mines,
        }
    }

    pub fn variants() -> &'static [Self] {
        &[Self::Easy, Self::Medium, Self::Hard, Self::Expert]
    }

    pub fn is_custom(&self) -> bool {
        match self {
            Self::Custom { .. } => true,
            _ => false,
        }
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            Self::Easy => "Easy",
            Self::Medium => "Medium",
            Self::Hard => "Hard",
            Self::Expert => "Expert",
            Self::Custom { .. } => "Custom",
        }
    }

    pub fn width(&self) -> usize {
        match self {
            Self::Easy => 8,
            Self::Medium => 16,
            Self::Hard => 48,
            Self::Expert => 64,
            Self::Custom { width, .. } => *width,
        }
    }

    pub fn height(&self) -> usize {
        match self {
            Self::Easy => 8,
            Self::Medium => 24,
            Self::Hard => 32,
            Self::Expert => 40,
            Self::Custom { height, .. } => *height,
        }
    }

    pub fn mines(&self) -> usize {
        match self {
            Self::Easy => 10,
            Self::Medium => 60,
            Self::Hard => 240,
            Self::Expert => 400,
            Self::Custom { mines, .. } => *mines,
        }
    }
}
