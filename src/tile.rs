use crate::adjecent;

#[derive(PartialEq, Copy, Clone)]
pub enum TileKind {
    Mine,
    Empty,
    Sign(u8),
}

#[derive(Copy, Clone)]
pub struct Tile {
    flagged: bool,
    revealed: bool,
    kind: TileKind,
}

impl Tile {
    pub fn new(width: usize, mines: &[bool], i: usize) -> Self {
        Self {
            flagged: false,
            revealed: false,
            kind: match mines.get(i).unwrap_or(&false) {
                true => TileKind::Mine,
                false => {
                    let mut sign = 0;

                    sign += adjecent::all(&mines, width, i)
                        .iter()
                        .filter(|entry| entry.map_or(false, |entry| *entry.1))
                        .count() as u8;

                    (sign > 0)
                        .then(|| TileKind::Sign(sign))
                        .unwrap_or(TileKind::Empty)
                }
            },
        }
    }

    pub fn toggle_flag(&mut self) -> bool {
        self.flagged = !self.flagged;
        self.flagged
    }

    pub fn flagged(&self) -> bool {
        self.flagged
    }

    pub fn reveal(&mut self) {
        self.flagged = false;
        self.revealed = true;
    }

    pub fn revealed(&self) -> bool {
        self.revealed
    }

    pub fn kind(&self) -> TileKind {
        self.kind
    }

    pub fn is_mine(&self) -> bool {
        match self.kind {
            TileKind::Mine => true,
            _ => false,
        }
    }

    pub fn is_empty(&self) -> bool {
        match self.kind {
            TileKind::Empty => true,
            _ => false,
        }
    }

    pub fn get_sign(&self) -> Option<u8> {
        match self.kind {
            TileKind::Sign(sign) => Some(sign),
            _ => None,
        }
    }
}
