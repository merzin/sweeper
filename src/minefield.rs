use crate::{adjecent, Omega, Tile};
use js_sys::Math;

pub struct Minefield {
    width: usize,
    tiles: Vec<Tile>,
    mine_count: usize,
    flag_count: usize,
}

impl Minefield {
    pub fn new(width: usize, height: usize, mine_count: usize, excluding: Option<usize>) -> Self {
        let tile_count = width * height;
        let mut mines = vec![];
        for _ in 0..tile_count {
            mines.push(false);
        }
        let mut available_tiles = (0..tile_count).collect::<Vec<usize>>();
        if let Some(excluding) = excluding {
            if excluding < tile_count {
                available_tiles.remove(excluding);
            }
        }
        for _ in 0..mine_count {
            mines
                .get_mut(
                    available_tiles
                        .remove((Math::random() * available_tiles.len() as f64).floor() as usize),
                )
                .map(|mine| *mine = true);
        }
        let mut tiles = vec![];
        for i in 0..tile_count {
            tiles.push(Tile::new(width, &mines, i));
        }
        Self {
            width,
            tiles,
            mine_count,
            flag_count: 0,
        }
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn tiles(&self) -> &[Tile] {
        &self.tiles
    }

    pub fn mine_count(&self) -> usize {
        self.mine_count
    }

    pub fn flag_count(&self) -> usize {
        self.flag_count
    }

    pub fn toggle_flag(&mut self, i: usize) {
        if let Some(tile) = self.tiles.get_mut(i) {
            if !tile.revealed() {
                match tile.toggle_flag() {
                    true => self.flag_count += 1,
                    false => self.flag_count -= 1,
                }
            }
        }
    }

    fn reveal_rec(&mut self, i: usize) {
        if self
            .tiles
            .get_mut(i)
            .map_or(true, |tile| match tile.flagged() {
                true => true,
                false => {
                    tile.reveal();
                    !tile.is_empty()
                }
            })
        {
            return;
        }

        let neighbors = adjecent::all(&self.tiles, self.width, i)
            .iter()
            .filter_map(|v| *v)
            .filter_map(|(neighbor, tile)| (!tile.revealed() && !tile.is_mine()).then(|| neighbor))
            .collect::<Vec<usize>>();

        for neighbor in neighbors {
            self.reveal_rec(neighbor);
        }
    }

    fn sign_neighbors(&mut self, i: usize) -> Option<Omega> {
        let sign = match self
            .tiles
            .get(i)
            .map(|tile| tile.revealed().then(|| tile.get_sign()))
            .flatten()
            .flatten()
        {
            Some(sign) => sign,
            None => return None,
        };

        let (flagged, unflagged) = {
            let mut flagged = vec![];
            let mut unflagged = vec![];
            for (neighbor, tile) in adjecent::all(&self.tiles, self.width, i)
                .iter()
                .filter_map(|v| *v)
                .filter(|(_, tile)| !tile.revealed())
            {
                match tile.flagged() {
                    true => flagged.push(neighbor),
                    false => unflagged.push(neighbor),
                }
            }
            (flagged, unflagged)
        };

        let mut omega = None;

        if flagged.len() == sign as usize {
            for neighbor in unflagged {
                let reveal_res = self.reveal(neighbor);
                if omega != Some(Omega::Lost) {
                    omega = reveal_res;
                }
            }
        }

        omega
    }

    fn has_won(&self) -> bool {
        self.tiles
            .iter()
            .find(|tile| tile.revealed() == tile.is_mine())
            .is_none()
    }

    pub fn reveal(&mut self, i: usize) -> Option<Omega> {
        let (mine, empty) = match self.tiles.get_mut(i) {
            Some(tile) => {
                if tile.revealed() {
                    if tile.get_sign().is_some() {
                        return self.sign_neighbors(i);
                    }
                    return None;
                }
                if tile.flagged() {
                    return None;
                }
                tile.reveal();
                (tile.is_mine(), tile.is_empty())
            }
            None => return None,
        };

        if !mine {
            let neighbors = adjecent::all(&self.tiles, self.width, i)
                .iter()
                .filter_map(|v| *v)
                .filter(|(_, tile)| !tile.revealed() && !tile.is_mine())
                .filter(|(_, tile)| empty || tile.is_empty())
                .map(|(neighbor, _)| neighbor)
                .collect::<Vec<usize>>();

            for neighbor in neighbors {
                self.reveal_rec(neighbor);
            }
        }

        match mine {
            true => Some(Omega::Lost),
            false => self.has_won().then(|| Omega::Won),
        }
    }
}
